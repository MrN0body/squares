#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

class Game:
    def __init__(self, size, player_names):
        players = []
        for player_name in player_names:
            players.append(Player(player_name))
        self._players = players
        self._field = Field(size, len(players))

        self._is_finished = False
        self._current_player_id = 0
    
    def getCurrentPlayer(self):
        return self._players[self._current_player_id]

    def start(self):
        while False == self._is_finished:
            self._field.visualize()
            self._is_finished = self.playNextTurn()

    def playNextTurn(self):
        dice_result = self.rollTheDice()
        self.visualizeDiceResult(dice_result,self.getCurrentPlayer())
        possible_squares = self._field.getPossibleSquares(self._current_player_id, dice_result)
        self.visualizePossibleSquares(possible_squares)
        if len(possible_squares) > 0:
            next_square = self._players[self._current_player_id].getSquareDecision(dice_result, possible_squares)
            self._field.addSquare(self._current_player_id, next_square)
            if True == self.checkFinishCondition():
                return True
        self._current_player_id += 1
        self._current_player_id %= len(self._players)
        return False

    def rollTheDice(self):
        return (random.randint(1,6), random.randint(1,6))

    def checkFinishCondition(self):
        return not self._field.checkSpaceLeft()

    def getScore(self):
        player_scores = {}
        for player_id in range(len(self._players)):
            player_score = self._field.getSquareSize(player_id)
            player_scores[player_id] = player_score
        return player_scores

    def getSortedScore(self):
        score = self.getScore()
        sorted_score = [ (player_score,player_id) for player_id,player_score in score.iteritems() ]
        sorted_score.sort(reverse=True)
        return sorted_score

    def __str__(self):
        sorted_score = self.getSortedScore()
        game_str = 'Square Game:'
        for player_score, player_id in sorted_score:
            game_str += '\n{}: {}'.format(self._players[player_id], player_score)
        return game_str
    
    def visualizeDiceResult(self, dice_result, playername):
        print('It is your turn, {}. You rolled a {} and {}'.format(playername,dice_result[0], dice_result[1]))

    def visualizePossibleSquares(self, possible_squares):
        if len(possible_squares) > 0:
            print('You have following {} options:'.format(len(possible_squares)))
            possible_squares.sort(key=lambda square: (square.getSize(), square.getPosition()))
            last_size=None
            possible_squares_str = ''
            for square_id, square in enumerate(possible_squares):
                if last_size != square.getSize():
                    last_size = square.getSize()
                    if possible_squares_str != '':
                        print(possible_squares_str)
                    possible_squares_str = 'Size {} Pos:'.format(last_size)
                else:
                    possible_squares_str += ', '
                possible_squares_str += '[{}]: {}'.format(square_id, square.getPosition())
            print(possible_squares_str)
        else:
            print('Skipping turn since no possible squares available.')

    def visualizeScore(self):
        print(self)

class Player:
    def __init__(self, name):
        self._name = name
    
    def getSquareDecision(self, dice_result, possible_squares):
        got_correct_input = False
        while False == got_correct_input:
            try:
                next_square_id = int(input())
            except ValueError:
                print('Not a number')
            if 0 <= next_square_id and next_square_id < len(possible_squares):
                got_correct_input = True
            else:
                print('Not in range')
        return possible_squares[next_square_id]

    def __str__(self):
        return self._name

class Field:
    def __init__(self, size, player_count):
        assert len(size) == 2
        assert size[0] > 0 and size[1] > 0
        assert  2 == player_count or 4 == player_count

        self._size = size
        row = [0] * size[0]
        self._field = [list(row) for _ in range(size[1])]
        self._start_positions = []
        self._start_positions.append((0,-1))
        self._start_positions.append((self._size[0]-1,self._size[1]))
        if 4 == player_count:
            self._start_positions.append((self._size[0]-1,-1))
            self._start_positions.append((0,self._size[1]))

        self._player_start_squares = {}
        for player_id in range(player_count):
            self.addSquare(player_id,Square(self._start_positions[player_id],(1,1),player_id))


    def getPossibleSquares(self, player_id, square_size):
        possible_squares = self._player_start_squares[player_id].getPossibleNeighbours(square_size, self._field, True)
        if square_size[0] != square_size[1]:
            square_size_reverse = (square_size[1], square_size[0])
            possible_squares += self._player_start_squares[player_id].getPossibleNeighbours(square_size_reverse, self._field, True)
        return possible_squares

    def addSquare(self, player_id, square):
        if player_id in self._player_start_squares:
            added_neighbour = self._player_start_squares[player_id].addNeighbour(square, self._field)
            assert True == added_neighbour
        else:
            self._player_start_squares[player_id] = square
        square.updateField(self._field)

    def checkSpaceLeft(self):
        minimal_square_size = (2,2)
        for player_id in range(len(self._start_positions)):
            possible_squares = self.getPossibleSquares(player_id, minimal_square_size)
            self.visualizeSpaceLeft(player_id, possible_squares)
            if len(possible_squares) <= 0:
                return False
        return True

    def getSquareSize(self, player_id):
        if self._player_start_squares[player_id] is None:
            return 0
        else:
            return self._player_start_squares[player_id].getTotalSquareSize()

    def getFieldStr(self, field):
        field_str = ''
        field_str += '\n ' + '-' * len(field[0])
        for row in field:
            field_str += '\n|'
            for element in row:
                if element <= 0:
                    field_str += ' '
                else:
                    field_str += str(element)
            field_str += '|'

        field_str += '\n ' + '-' * len(field[0])
        return field_str

    def __str__(self):
        field_str = 'Field:'
        field_str += self.getFieldStr(self._field)
        return field_str

    def getVisualizeStrings(self):
        start_row = '╔'
        start_row += '═' * (self._size[0])
        start_row += '╗'
        row = '║'
        row += ' ' * (self._size[0])
        row += '║'
        end_row = '╚'
        end_row += '═' * (self._size[0])
        end_row += '╝'
        field_strs = [start_row] + [str(row) for _ in range(self._size[1])] + [end_row]
        return field_strs

    def visualize(self):
        print(self)
        # field_vis_strs = self.getVisualizeStrings()
        # for player_id in self._player_start_squares:
        #     for square in self._player_start_squares[player_id].getNeighbours(True):
        #         position = square.getPosition()
        #         square_vis_strs = square.getVisualizeStrings()
        #         print(square_vis_strs)
        #         for y_index, square_vis_str in enumerate(square_vis_strs):
        #             before = field_vis_strs[position[1]+y_index+1][:len('║')+position[0]]
        #             after = field_vis_strs[position[1]+y_index+1][position[0]+len(square_vis_str)+len('║'):]
        #             field_vis_strs[position[1]+y_index+1] = before + square_vis_str + after
        # print('Field:')
        # for field_vis_str in field_vis_strs:
        #     print(field_vis_str)
    
    def visualizeSpaceLeft(self, player_id, possible_squares):
        print('Player {} has {} possible (2,2) squares left'.format(player_id,len(possible_squares)))

class Square:
    def __init__(self, position, size, player_id):
        assert len(position) == 2
        assert len(size) == 2

        self._position = position
        self._size = size
        self._player_id = player_id
        self._neighbours = []

    def getPosition(self):
        return self._position

    def getSize(self):
        return self._size

    def getSquareSize(self):
        return self._size[0] * self._size[1]

    def addNeighbour(self, new_neighbour, field):
        new_neighbour_size = new_neighbour.getSize()
        possible_neighbours = self.getPossibleNeighbours(new_neighbour_size, field, False)
        for possible_neighbour in possible_neighbours:
            if new_neighbour == possible_neighbour:
                self._neighbours.append(new_neighbour)
                return True
        for neighbour in self._neighbours:
            if True == neighbour.addNeighbour(new_neighbour, field):
                return True
        return False

    def getNeighbours(self, include_neighbours=False):
        for neighbour in self._neighbours:
            yield neighbour
            if True == include_neighbours:
                for neighbours_neighbour in neighbour.getNeighbours(include_neighbours):
                    yield neighbours_neighbour

    def getBoundingBox(self):
        position = self.getPosition()
        size = self.getSize()
        xmin, xmax = position[0],position[0]+size[0]-1
        ymin, ymax = position[1],position[1]+size[1]-1
        return (xmin, xmax, ymin, ymax)

    def getInlayerIterator(self):
        (xmin, xmax, ymin, ymax) = self.getBoundingBox()
        for x in range(xmin, xmax+1):
            for y in range(ymin, ymax+1):
                yield (x,y)

    def getEdgeIterator(self, field):
        (xmin, xmax, ymin, ymax) = self.getBoundingBox()
        for x in range(xmin-1,xmax+2):
            for y in range(ymin-1,ymax+2):
                if \
                    (x == xmin-1 and y == ymin-1) or \
                    (x == xmax+1 and y == ymin-1) or \
                    (x == xmin-1 and y == ymax+1) or \
                    (x == xmax+1 and y == ymax+1) or \
                    x < 0 or len(field[0]) <= x or \
                    y < 0 or len(field) <= y:
                    continue
                yield (x,y)

    def getPossibleNeighbours(self, square_size, field, include_neighbours=False):
        possible_neighbour_set_positions = []
        for (x,y) in self.getEdgeIterator(field):
            if \
                0 < field[y][x] or \
                (self._player_id+1) * -1 != field[y][x]:
                continue
            possible_neighbour_set_positions.append((x,y))
        possible_neighbour_position_shifts = list(set(\
            [(dx,0) for dx in range(square_size[0])] + \
            [(0,dy) for dy in range(square_size[1])] + \
            [(dx,square_size[1]-1) for dx in range(square_size[0])] + \
            [(square_size[0]-1,dy) for dy in range(square_size[1])]))
        possible_neighbours = []
        for (x,y) in possible_neighbour_set_positions:
            for (dx, dy) in possible_neighbour_position_shifts:
                possible_neighbour = Square((x-dx,y-dy), square_size, self._player_id)
                if True == possible_neighbour.checkIsInFreeSpot(field):
                    possible_neighbours.append(possible_neighbour)
        if True == include_neighbours:
            for neighbour in self._neighbours:
                neighbour_possible_neighbours = neighbour.getPossibleNeighbours(square_size, field, True)
                possible_neighbours = list(set(possible_neighbours + neighbour_possible_neighbours))
        return possible_neighbours

    def checkIsInFreeSpot(self, field):
        (xmin, xmax, ymin, ymax) = self.getBoundingBox()
        if xmin < 0 or ymin < 0 or len(field[0]) <= xmax or len(field) <= ymax:
            return False
        for (x,y) in self.getInlayerIterator():
            if not( \
                0 == field[y][x] or \
                (self._player_id + 1) * -1 == field[y][x]
                ):
                return False
        for (x,y) in self.getEdgeIterator(field):
            if not( \
                0 == field[y][x] or \
                (self._player_id + 1) * -1 == field[y][x] or \
                (self._player_id + 1) == field[y][x]
                ):
                return False
        return True

    def updateField(self, field):
        for (x,y) in self.getEdgeIterator(field):
            if x<0 or len(field[0]) <= x or y<0 or len(field) <= y or 0 < field[y][x]:
                continue
            field[y][x] = (self._player_id + 1) * -1
        for (x,y) in self.getInlayerIterator():
            if x<0 or len(field[0]) <= x or y<0 or len(field) <= y:
                continue
            field[y][x] = self._player_id + 1

    def __str__(self):
        return 'Pos: {}, Size: {}'.format(self.getPosition(),self.getSize())

    def __eq__(self, other):
        position = self.getPosition()
        size = self.getSize()
        other_position = other.getPosition()
        other_size = self.getSize()
        return position == other_position and size == other_size

    def __hash__(self):
        return hash((self.getPosition(),self.getSize()))

    def getTotalSquareSize(self):
        total_size = self.getSquareSize()
        for neighbour in self._neighbours:
            total_size += neighbour.getTotalSquareSize()
        return total_size

    def getVisualizeStrings(self):
        size = self.getSize()
        vis_strs = []
        if size[0] <= 2 or size[1] <= 2:
            for y in range(size[1]):
                vis_str = ''
                for x in range(size[0]):
                    vis_str += str(self._player_id)
                vis_strs.append(vis_str)
        else:
            for y in range(size[1]):
                vis_str = ''
                if 0 == y:
                    vis_str += '┌'
                    for x in range(1,size[0]-1):
                        vis_str += '─'
                    vis_str += '┐'
                elif size[1]-1 == y:
                    vis_str += '└'
                    for x in range(1,size[0]-1):
                        vis_str += '─'
                    vis_str += '┘'
                else:
                    vis_str += '│'
                    for x in range(1,size[0]-1):
                        vis_str += str(self._player_id)
                    vis_str += '│'
                vis_strs.append(vis_str)
        return vis_strs

if '__main__' == __name__:
    game = Game((30,30),['Clemens','Sandra'])
    game.start()
    game.visualizeScore()
